Tap Follower from ½" and ⅜" stock
=================================

This design uses ½" and ¾" cold-rolled steel stock, and a 5.55mm
x 38.1mm compression spring with a 12mm minimum length. I designed
for 25mm stroke.  It is not very parametric and was created only
so that I could validate a project I started to freehand.  Also,
I prefer to design in metric but have only imperial stock, so
this is a confusing combination of imperial and metric measurements,
and I don't know how to choose the system for individual dimensions
in FreeCAD.

I wrote [build notes](https://forum.makerforums.info/t/shop-made-spring-loaded-tap-follower/85734?u=mcdanlj)
that describe how I made this.

There is no need for a close fit for the head of the follower pin.
It could just be drilled. The only close sliding fit required is
for the body of the follower pin itself, which I made 5/16".

A spring that is a close outside fit for the follower cap would
not need a pin. However, the pin gives a positive stop before
buckling the spring, so it will be easy to safely load this to
maximum compression to start tapping.

I recommend using this as a study to design your own rather than
a project to replicate.
